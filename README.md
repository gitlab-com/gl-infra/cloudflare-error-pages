# Cloudflare Custom Pages

These pages are used to populate the Cloudflare custom pages for blocks, errors,
and challenges, etc.

If you make an update to these files, you will need to edit the Cloudflare
custom pages and re-publish them. Otherwise they will not change.
